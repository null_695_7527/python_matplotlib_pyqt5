#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : 4.array_study.py
@Author  : 神秘藏宝室
@Time    : 2020/2/5 11:33
版权店铺 : https://ilovemcu.taobao.com
"""

'''
ndarray.shape
ndarray.shape 表示数组的维度，返回一个元组，这个元组的长度就是维度的数目，
即 ndim 属性(秩)。比如，一个二维数组，其维度表示"行数"和"列数"。
ndarray.shape 也可以用于调整数组大小。
'''
import numpy as np

a = np.array([[1, 2, 3], [4, 5, 6]])
print(a.shape)  # 相当于返回行列数

# 定义3*2的数组，数值没初始化是随机的
import numpy as np
x = np.empty([3,2], dtype = int)
print (x)


import numpy as np
# 默认为浮点数
x = np.zeros(5)
print(x)
# 设置类型为整数
y = np.zeros((5,), dtype=np.int)
print(y)
y = np.zeros((5), dtype=np.int)
print(y)
y = np.zeros(5, dtype=np.int)
print(y)
# 自定义类型
print("# 自定义类型")
student = np.zeros((2, 3), dtype=[('age', 'i4'), ('name', 'S4')])
print(student)
print(student['age'])
print(student['name'])      #测试看字符用zeros初始化成了空字符串
print(type(student['name']))

'''
numpy.ones
创建指定形状的数组，数组元素以 1 来填充：
'''
import numpy as np
student = np.ones((2, 3), dtype=[('age', 'i4'), ('name', 'S4')])
print(student)
print(student['age'])
print(student['name'])      #测试看字符用ones初始化成了字符1
