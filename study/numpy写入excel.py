#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : numpy写入excel.py
@Author  : 神秘藏宝室
@Time    : 2020/2/9 20:25
店铺     : https://ilovemcu.taobao.com
"""

# pip install pandas -i https://pypi.doubanio.com/simple

import numpy as np
import pandas as pd
import chardet

str = 'test'
A = np.array([[str, "2"], [3, 2], [1, 1], [3, 5], [5, 2]])


mySensorDataType = np.dtype(
    [('nums', 'i4'), ('times', np.unicode , 40), ('runTime', 'f4'), ('A0', 'i4'), ('A1', 'i4'), ('A2', 'i4'), ('A3', 'i4'),
     ('A4', 'i4'), ('A5', 'i4'),
     ('A6', 'i4'), ('A7', 'i4'), ('A8', 'i4'), ('A9', 'i4'), ('A10', 'i4'),
     ('A11', 'i4')])
pressPoints = np.array([(0, '2020-02-02', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)],
                                      dtype=mySensorDataType)
print(pressPoints["times"],type(pressPoints['times'][0]))

arr1 = np.array(['0','1'],dtype=np.unicode)

print(arr1,type(arr1[0]),arr1[0])


data = pd.DataFrame(pressPoints)
writer = pd.ExcelWriter('A.xls')  # 写入Excel文件
data.to_excel(writer, 'sheet1')  # ‘page_1’是写入excel的sheet名
writer.save()

writer.close()
