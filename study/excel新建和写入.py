#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : excel新建和写入.py
@Author  : 神秘藏宝室
@Time    : 2020/2/9 18:40
店铺     : https://ilovemcu.taobao.com
"""
import xlwt
import datetime


class MyExcelHelp(object):
    def write_excel_xls(path, sheet_name, value):
        index = len(value)  # 获取需要写入数据的行数(value是个二维数组)
        try:
            workbook = xlwt.Workbook()  # 新建一个工作簿
            sheet = workbook.add_sheet(sheet_name)  # 在工作簿中新建一个表格
            for i in range(0, index):
                for j in range(0, len(value[i])):
                    sheet.write(i, j, value[i][j])  # 像表格中写入数据（对应的行和列）
            workbook.save(path)  # 保存工作簿
            print("xls格式表格写入数据成功！")
        except Exception as e:
            print(e)  # 输出异常行为名称


if __name__ == '__main__':
    # 保存到当前工程目录
    dt = str(datetime.datetime.now()).replace(':', '').replace(' ','_')
    print(dt, type(dt))
    book_name_xls = '压力曲线图' + dt + '.xls'
    print(book_name_xls, type(book_name_xls))
    sheet_name_xls = '表格1'
    value_title = [["姓名", "性别", "年龄", "城市", "职业"],
                   ["111", "女", "66", "石家庄", "运维工程师"],
                   ["222", "男", "55", "南京", "饭店老板"],
                   ["333", "女", "27", "苏州", "保安"]]
    MyExcelHelp.write_excel_xls(book_name_xls, sheet_name_xls, value_title)
