#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : excel读取文件内容.py
@Author  : 神秘藏宝室
@Time    : 2020/2/9 18:56
店铺     : https://ilovemcu.taobao.com
"""
import xlrd



def read_excel_xls(path):
    workbook = xlrd.open_workbook(path)  # 打开工作簿
    sheets = workbook.sheet_names()  # 获取工作簿中的所有表格
    worksheet = workbook.sheet_by_name(sheets[0])  # 获取工作簿中所有表格中的的第一个表格
    for i in range(0, worksheet.nrows):
        for j in range(0, worksheet.ncols):
            print(worksheet.cell_value(i, j), "\t", end="")  # 逐行逐列读取数据
        print()


if __name__ == '__main__':
    book_name_xls = 'xls格式测试工作簿.xls'
    read_excel_xls(book_name_xls)