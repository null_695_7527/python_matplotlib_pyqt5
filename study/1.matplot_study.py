#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : 1.matplot_study.py
@Author  : 神秘藏宝室
@Time    : 2020/2/5 10:16
版权店铺 : https://ilovemcu.taobao.com
"""
"""
学习记录：
可以看到这个显示的图的组件本身还有一些功能比如跟随鼠标显示坐标值，
放大局部图像等。
然后我们来看看这段代码。首先x是numpy中的一个array对象，
其内容是-1到1之间均等地取了大约50个值。
这些值其实就是我们图中后来的横坐标。
然后通过array加减乘除时特别的性质，
可以将类似于函数表达式那样的式子写在代码里。即2*x + 1，
很接近函数表达式的y=2x+1了。
那么y也是一个array，一一对应x中各个值进行2*x+1后的值。
这样的一个array，x和array y，两个坐标集组成了一幅图。
那么如何将这幅图显示出来，用到的就是后三行代码。
"""

import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(-1,1,50)
y = 2*x + 1

plt.figure()
plt.plot(x,y)
plt.show()