#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : 11.numpy_matplot2.py
@Author  : 神秘藏宝室
@Time    : 2020/2/5 15:58
店铺     : https://ilovemcu.taobao.com
"""

import numpy as np
from matplotlib import pyplot as plt

# 如果是在 PyCharm 里，只要下面一行，上面的一行可以删除
plt.rcParams['font.sans-serif'] = ['SimHei']
# 用来正常显示负号
plt.rcParams['axes.unicode_minus'] = False

# 计算正弦和余弦曲线上的点的 x 和 y 坐标
x = np.arange(0, 3 * np.pi, 0.1)
y_sin = np.sin(x)
y_cos = np.cos(x)
# 建立 subplot 网格，高为 2，宽为 1
# 绘制
plt.title('Sin&Cos曲线')
plt.plot(x, y_sin, label='sinx曲线')
plt.plot(x, y_cos, '.r',label='cosx曲线')
plt.xlabel("x axis caption")
plt.legend()  #显示上面的label
plt.xlabel('x坐标')
plt.ylabel('f(x)')
plt.axis([0, 2*np.pi, -1, 1])#设置坐标范围axis([xmin,xmax,ymin,ymax])
#plt.ylim(-1,1)#仅设置y轴坐标范围
plt.show()
