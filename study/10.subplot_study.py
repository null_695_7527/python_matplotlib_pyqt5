#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : 10.subplot_study.py
@Author  : 神秘藏宝室
@Time    : 2020/2/5 15:34
店铺     : https://ilovemcu.taobao.com
"""
# 练习
import numpy as np
from matplotlib import pyplot as plt

# 如果是在 PyCharm 里，只要下面一行，上面的一行可以删除
plt.rcParams['font.sans-serif'] = ['SimHei']
# 用来正常显示负号
plt.rcParams['axes.unicode_minus'] = False

# 计算正弦和余弦曲线上的点的 x 和 y 坐标
x = np.arange(0,  3  * np.pi,  0.1)
y_sin = np.sin(x)
y_cos = np.cos(x)
# 建立 subplot 网格，高为 2，宽为 1
# 激活第一个 subplot
plt.subplot(2,  1,  1)
# 绘制第一个图像
plt.plot(x, y_sin)
plt.xlabel("x axis caption")
plt.title('Sin曲线')
# 将第二个 subplot 激活，并绘制第二个图像
plt.subplot(2,  1,  2)
plt.plot(x, y_cos, '.r')
plt.title('Cos曲线')
plt.xlabel("x axis caption")
# 展示图像
plt.show()