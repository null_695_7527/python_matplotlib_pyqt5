#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : 2.numpy_study.py
@Author  : 神秘藏宝室
@Time    : 2020/2/5 10:43
版权店铺 : https://ilovemcu.taobao.com
"""
"""
发现matplot用到了numpy，所以开始学习numpy
学习资料地址：https://www.runoob.com/numpy/numpy-ndarray-object.html
"""

"""
numpy.array(object, dtype = None, copy = True, order = None, subok = False, ndmin = 0)
object	数组或嵌套的数列
dtype	数组元素的数据类型，可选
copy	对象是否需要复制，可选
order	创建数组的样式，C为行方向，F为列方向，A为任意方向（默认）
subok	默认返回一个与基类类型一致的数组
ndmin	指定生成数组的最小维度
"""
import numpy as np
a = np.array([1,2,3])
'''
<module 'numpy' from 'C:\\Python3\\lib\\site-packages\\numpy\\__init__.py'> <class 'module'> [1 2 3] <class 'numpy.ndarray'>
'''
print(np, type(np), a, type(a))

print("# 多于一个维度")
a = np.array([[1,  2],  [3,  4]])
print(np, type(np), a, type(a))

print("# 最小维度")
a = np.array([1,  2,  3, 4, 5], ndmin=3)   # 最小是1，测试发现0和1一样，简单理解一个维度一个中括号
print(a)

print("#dtype 参数")
a = np.array([1,  2,  3], dtype=complex)
print(a)

'''
bool_	布尔型数据类型（True 或者 False）
int_	默认的整数类型（类似于 C 语言中的 long，int32 或 int64）
intc	与 C 的 int 类型一样，一般是 int32 或 int 64
intp	用于索引的整数类型（类似于 C 的 ssize_t，一般情况下仍然是 int32 或 int64）
int8	字节（-128 to 127）
int16	整数（-32768 to 32767）
int32	整数（-2147483648 to 2147483647）
int64	整数（-9223372036854775808 to 9223372036854775807）
uint8	无符号整数（0 to 255）
uint16	无符号整数（0 to 65535）
uint32	无符号整数（0 to 4294967295）
uint64	无符号整数（0 to 18446744073709551615）
float_	float64 类型的简写
float16	半精度浮点数，包括：1 个符号位，5 个指数位，10 个尾数位
float32	单精度浮点数，包括：1 个符号位，8 个指数位，23 个尾数位
float64	双精度浮点数，包括：1 个符号位，11 个指数位，52 个尾数位
complex_	complex128 类型的简写，即 128 位复数
complex64	复数，表示双 32 位浮点数（实数部分和虚数部分）
complex128	复数，表示双 64 位浮点数（实数部分和虚数部分）
'''
