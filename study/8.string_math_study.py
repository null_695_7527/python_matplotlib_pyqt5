#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : 8.string_math_study.py
@Author  : 神秘藏宝室
@Time    : 2020/2/5 14:26
店铺     : https://ilovemcu.taobao.com
"""

'''
NumPy 字符串函数
以下函数用于对 dtype 为 numpy.string_ 或 numpy.unicode_ 的数组执行向量化字符串操作。 它们基于 Python 内置库中的标准字符串函数。

这些函数在字符数组类（numpy.char）中定义。

函数	描述
add()	对两个数组的逐个字符串元素进行连接
multiply()	返回按元素多重连接后的字符串
center()	居中字符串
capitalize()	将字符串第一个字母转换为大写
title()	将字符串的每个单词的第一个字母转换为大写
lower()	数组元素转换为小写
upper()	数组元素转换为大写
split()	指定分隔符对字符串进行分割，并返回数组列表
splitlines()	返回元素中的行列表，以换行符分割
strip()	移除元素开头或者结尾处的特定字符
join()	通过指定分隔符来连接数组中的元素
replace()	使用新字符串替换字符串中的所有子字符串
decode()	数组元素依次调用str.decode
encode()	数组元素依次调用str.encode
'''

import numpy as np

print('连接两个字符串：')
print(np.char.add(['hello'], [' xyz']))
print('\n')

print('连接示例：')
print(np.char.add(['hello', 'hi'], [' abc', ' xyz']))

'''
numpy.char.center()
numpy.char.center() 函数用于将字符串居中，并使用指定字符在左侧和右侧进行填充。
'''
import numpy as np

# np.char.center(str , width,fillchar) ：
# str: 字符串，width: 长度，fillchar: 填充字符
print(np.char.center('Runoob', 20, fillchar='*'))

'''
numpy.char.split()
numpy.char.split() 通过指定分隔符对字符串进行分割，并返回数组。默认情况下，分隔符为空格。
'''
import numpy as np

# 分隔符默认为空格
print(np.char.split('i like runoob?'))
# 分隔符为 .
print(np.char.split('www.runoob.com', sep='.'))
# 分隔符为 ,
s = '$GPGGA,0,1,2,3,4,5,6,7,8\r\n'
print(np.char.split(s, sep=','))

'''
numpy.char.strip()
numpy.char.strip() 函数用于移除开头或结尾处的特定字符。
'''

import numpy as np

# 移除字符串头尾的 a 字符
print(np.char.strip('ashok arunooba', 'a'))
# 移除数组元素头尾的 a 字符
print(np.char.strip(['arunooba', 'admin', 'java'], 'a'))
# 移除$ \r\n
s = '$GPGGA,0,1,2,3,4,5,6,7,8\r\n'
s1 = np.char.strip(s, '$')
print(s1, type(s1))
s2 = np.char.strip(s1, '\r\n')
print(s2, type(s2))
# 汇总
s3 = np.char.strip(np.char.strip(s, '$'), "\r\n")
print(s3, type(s3))
print(str(s3), type(str(s3)))       #测试解析GPS的NMEA数据时候把某一帧的各个数据解析

'''
numpy.char.encode()
numpy.char.encode() 函数对数组中的每个元素调用 str.encode 函数。 
默认编码是 utf-8，可以使用标准 Python 库中的编解码器。
'''
import numpy as np
a = np.char.encode('runoob', 'cp500')
print(a)

'''
numpy.char.decode()
numpy.char.decode() 函数对编码的元素进行 str.decode() 解码。
'''
import numpy as np

a = np.char.encode('runoob', 'cp500')
print(a)
print(np.char.decode(a, 'cp500'))

'''
NumPy 统计函数
NumPy 提供了很多统计函数，用于从数组中查找最小元素，最大元素，百分位标准差和方差等。 函数说明如下：

numpy.amin() 和 numpy.amax()
numpy.amin() 用于计算数组中的元素沿指定轴的最小值。

numpy.amax() 用于计算数组中的元素沿指定轴的最大值。
'''
import numpy as np

a = np.array([[3, 7, 5], [8, 4, 3], [2, 4, 9]])
print('我们的数组是：')
print(a)
print('\n')
print('调用 amin() 函数：')
print(np.amin(a, 1))
print('\n')
print('再次调用 amin() 函数：')
print(np.amin(a, 0))
print('\n')
print('调用 amax() 函数：')
print(np.amax(a))
print('\n')
print('再次调用 amax() 函数：')
print(np.amax(a, axis=0))


'''
savetxt()
savetxt() 函数是以简单的文本文件格式存储数据，对应的使用 loadtxt() 函数来获取数据。

np.loadtxt(FILENAME, dtype=int, delimiter=' ')
np.savetxt(FILENAME, a, fmt="%d", delimiter=",")
'''
# 测试savetxt
import numpy as np

a = np.array([2, 2, 3, 4, 5],dtype = int)
print(a)
np.savetxt('out.txt', a)
b = np.loadtxt('out.txt')
print(b)

arr1 = np.array(['12', '22', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'])
print(arr1,type(arr1))
arr2 = arr1.astype(np.float)
print(arr2,type(arr2))

str1= '12,22,3,4,5,6,7,8,9,10,11,12'
a = np.char.split(str1, sep=',')
print(a)
a = np.fromstring(str1,dtype=int,sep=',')
print(a)