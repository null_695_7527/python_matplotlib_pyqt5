#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : 9.numpy_matplot.py
@Author  : 神秘藏宝室
@Time    : 2020/2/5 15:14
店铺     : https://ilovemcu.taobao.com
"""

'''
终于可以回来学matplot了
'''

# 练习
import numpy as np
from matplotlib import pyplot as plt

#为了设置显示汉字
# # 在我的 notebook 里，要设置下面两行才能显示中文
# plt.rcParams['font.family'] = ['sans-serif']
# 如果是在 PyCharm 里，只要下面一行，上面的一行可以删除
plt.rcParams['font.sans-serif'] = ['SimHei']

x = np.arange(0, 10000,100)
# print(x)
y = 2 * x + 5
plt.title("压力和阻值的曲线")
plt.xlabel("压力(g)")
plt.ylabel("阻值(kΩ)")
# plt.plot(x, y)  #默认实线和蓝色
plt.plot(x, y, '.r') #点和红色
plt.show()




