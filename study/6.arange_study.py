#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : 6.arange_study.py
@Author  : 神秘藏宝室
@Time    : 2020/2/5 13:33
店铺     : https://ilovemcu.taobao.com
"""

'''
NumPy 从数值范围创建数组
这一章节我们将学习如何从数值范围创建数组。

numpy.arange
numpy 包中的使用 arange 函数创建数值范围并返回 ndarray 对象，函数格式如下：

numpy.arange(start, stop, step, dtype)
根据 start 与 stop 指定的范围以及 step 设定的步长，生成一个 ndarray。

参数说明：

参数	描述
start	起始值，默认为0
stop	终止值（不包含）
step	步长，默认为1
dtype	返回ndarray的数据类型，如果没有提供，则会使用输入数据的类型。
'''

# 生成 0 到 5 的数组:
import numpy as np

x = np.arange(5)
print(x)

# 设置返回类型位 float:
import numpy as np

# 设置了 dtype
x = np.arange(5, dtype=float)
print(x)

import numpy as np
x = np.arange(10, 20, 2)  #10到20，每加2选择一个数字，不包括20
print (x)

'''
numpy.linspace
numpy.linspace 函数用于创建一个一维数组，数组是一个等差数列构成的，格式如下：

np.linspace(start, stop, num=50, endpoint=True, retstep=False, dtype=None)
'''
#以下实例用到三个参数，设置起始点为 1 ，终止点为 10，数列个数为 10。
import numpy as np
a = np.linspace(1, 10, 10)
print(a)

