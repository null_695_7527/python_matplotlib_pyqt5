#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
@File    : 7.array_study.py
@Author  : 神秘藏宝室
@Time    : 2020/2/5 13:53
店铺     : https://ilovemcu.taobao.com
"""
'''
NumPy 切片和索引
ndarray对象的内容可以通过索引或切片来访问和修改，与 Python 中 list 的切片操作一样。

ndarray 数组可以基于 0 - n 的下标进行索引，切片对象可以通过内置的 slice 函数，
并设置 start, stop 及 step 参数进行，
从原数组中切割出一个新数组。
'''
import numpy as np

a = np.arange(10, 20, 1)
print(a)
s = slice(2, 7, 2)  # 从索引 2 开始到索引 7 停止，间隔为2
print(a[s])

import numpy as np

a = np.arange(10)
b = a[2:7:2]  # 从索引 2 开始到索引 7 停止，间隔为 2
print(b)

'''
冒号 : 的解释：如果只放置一个参数，如 [2]，将返回与该索引相对应的单个元素。如果为 [2:]，
表示从该索引开始以后的所有项都将被提取。如果使用了两个参数，如 [2:7]，
那么则提取两个索引(不包括停止索引)之间的项。
'''
import numpy as np

a = np.arange(10)  # [0 1 2 3 4 5 6 7 8 9]
b = a[5]
print(b)  # 5
b = a[5:]
print(b)  # [5 6 7 8 9]
b = a[:3]
print(b)  # [0 1 2]
b = a[2:5]
print(b)  # [2 3 4]

# 多维数组同样适用上述索引提取方法：
import numpy as np

a = np.array([[1, 2, 3], [3, 4, 5], [4, 5, 6]])
print(a)
# 从某个索引处开始切割
print('从数组索引 a[1:] 处开始切割')
print(a[1:])

# 切片还可以包括省略号 …，来使选择元组的长度与数组的维度相同。 如果在行位置使用省略号，它将返回包含行中元素的 ndarray
import numpy as np

print("=================")
a = np.array([[1, 2, 3], [3, 4, 5], [4, 5, 6], [7, 8, 9]])
print(a)
print(a[1])  # 第2行元素
print(a[..., 1])  # 第2列元素
print(a[1, ...])  # 第2行元素
print(a[..., 1:])  # 第2列及剩下的所有元素


'''
NumPy 迭代数组
NumPy 迭代器对象 numpy.nditer 提供了一种灵活访问一个或者多个数组元素的方式。

迭代器最基本的任务的可以完成对数组元素的访问。

接下来我们使用 arange() 函数创建一个 2X3 数组，并使用 nditer 对它进行迭代。
'''
import numpy as np

a = np.arange(6).reshape(2, 3)
print('原始数组是：')
print(a)
print('\n')
print('迭代输出元素：')
for x in np.nditer(a):
    print(x, end=", ")
print('\n')

# 笔记，字符串分割
import numpy as np
s = b'$GPGGA,0,1,2,3,4,5,6,7,8\r\n'
a = np.frombuffer(s, dtype='S1')
print(a)
for x in np.nditer(a):
    print(x, end=", ")
print('\n')

'''
修改数组中元素的值
nditer 对象有另一个可选参数 op_flags。 默认情况下，nditer 将视待迭代遍历的数组为只读对象（read-only），
为了在遍历数组的同时，实现对数组元素值得修改，必须指定 read-write 或者 write-only 的模式。
'''
import numpy as np

a = np.arange(0, 60, 5)
a = a.reshape(3, 4)
print('原始数组是：')
print(a)
print('\n')
for x in np.nditer(a, op_flags=['readwrite']):
    x = 2 * x
print('修改后的数组是：')
print(a)
