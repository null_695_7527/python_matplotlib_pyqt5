# python matplotlib pyqt5绘图和界面学习

#### 介绍
编写project2，功能：测试学习网上4种绘图方式，带动态显示
源自以下博客
https://blog.csdn.net/weixin_43008870/article/details/92441288
#### 界面

![](https://img.alicdn.com/imgextra/i1/63891318/O1CN01SdWvrC1Lbgb8s9Q4j_!!63891318.jpg)

#### 软件架构
代码使用环境
```
C:\Users\Administrator>pip list
Package         Version
--------------- ----------
-scintilla      2.11.4
altgraph        0.17
Click           7.0
cycler          0.10.0
future          0.18.2
kiwisolver      1.1.0
matplotlib      3.1.3
numpy           1.18.1
packaging       20.1
pefile          2019.4.18
pip             20.0.2
PyInstaller     3.6
pyparsing       2.4.6
PyQt5           5.14.1
PyQt5-sip       12.7.1
pyqt5-tools     5.13.0.1.5
PyQtChart       5.14.0
PyQtWebEngine   5.14.0
pyserial        3.4
python-dateutil 2.8.1
python-dotenv   0.10.5
pywin32         227
pywin32-ctypes  0.2.0
QScintilla      2.11.4
setuptools      41.2.0
sip             4.19.8
six             1.14.0
toml            0.10.0
```

####安装库
```
pip install matplotlib -i https://pypi.doubanio.com/simple
```
成功安装以下各版本
```
Installing collected packages: numpy, python-dateutil, kiwisolver, cycler, matplotlib
Successfully installed cycler-0.10.0 kiwisolver-1.1.0 matplotlib-3.1.3 numpy-1.18.1 python-dateutil-2.8.1
```



