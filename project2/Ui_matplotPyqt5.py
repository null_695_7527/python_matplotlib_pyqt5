# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'I:\1.MySourceCode\PythonCode\myPython\PyQtCode\4.matplot\matplotPyqt5.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 752)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.centralwidget)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.lineDisplayGB = QtWidgets.QGroupBox(self.centralwidget)
        self.lineDisplayGB.setObjectName("lineDisplayGB")
        self.verticalLayout.addWidget(self.lineDisplayGB)
        self.imageDisplayGB = QtWidgets.QGroupBox(self.centralwidget)
        self.imageDisplayGB.setObjectName("imageDisplayGB")
        self.verticalLayout.addWidget(self.imageDisplayGB)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.barDisplayGB = QtWidgets.QGroupBox(self.centralwidget)
        self.barDisplayGB.setObjectName("barDisplayGB")
        self.verticalLayout_2.addWidget(self.barDisplayGB)
        self.d3DisplayGB = QtWidgets.QGroupBox(self.centralwidget)
        self.d3DisplayGB.setObjectName("d3DisplayGB")
        self.verticalLayout_2.addWidget(self.d3DisplayGB)
        self.horizontalLayout.addLayout(self.verticalLayout_2)
        self.horizontalLayout_2.addLayout(self.horizontalLayout)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 23))
        self.menubar.setObjectName("menubar")
        self.menu = QtWidgets.QMenu(self.menubar)
        self.menu.setObjectName("menu")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionShop = QtWidgets.QAction(MainWindow)
        self.actionShop.setObjectName("actionShop")
        self.menu.addAction(self.actionShop)
        self.menubar.addAction(self.menu.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "PYQT5+matplotlit测试显示曲线图@神秘藏宝室"))
        self.lineDisplayGB.setTitle(_translate("MainWindow", "line"))
        self.imageDisplayGB.setTitle(_translate("MainWindow", "Image"))
        self.barDisplayGB.setTitle(_translate("MainWindow", "Bar"))
        self.d3DisplayGB.setTitle(_translate("MainWindow", "3D"))
        self.menu.setTitle(_translate("MainWindow", "关于"))
        self.actionShop.setText(_translate("MainWindow", "Shop"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
