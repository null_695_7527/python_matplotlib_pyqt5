# python matplotlib pyqt5绘图和界面学习

#### 介绍
编写project3，功能：pyqt界面点击一下按键就产生一个数据，为了以后做压力曲线输出做准备。把12路压力数据用numpy数据格式处理的测试
目前先用到A0这一路的数据，写的随机值，现在出曲线图。

#### 界面

![](https://img.alicdn.com/imgextra/i3/63891318/O1CN01dpYvcw1LbgbFE9BSd_!!63891318.jpg)

#### 软件架构
代码使用环境
```
C:\Users\Administrator>pip list
Package         Version
--------------- ----------
-scintilla      2.11.4
altgraph        0.17
Click           7.0
cycler          0.10.0
future          0.18.2
kiwisolver      1.1.0
matplotlib      3.1.3
numpy           1.18.1
packaging       20.1
pefile          2019.4.18
pip             20.0.2
PyInstaller     3.6
pyparsing       2.4.6
PyQt5           5.14.1
PyQt5-sip       12.7.1
pyqt5-tools     5.13.0.1.5
PyQtChart       5.14.0
PyQtWebEngine   5.14.0
pyserial        3.4
python-dateutil 2.8.1
python-dotenv   0.10.5
pywin32         227
pywin32-ctypes  0.2.0
QScintilla      2.11.4
setuptools      41.2.0
sip             4.19.8
six             1.14.0
toml            0.10.0
```

####安装库
```
pip install matplotlib -i https://pypi.doubanio.com/simple
```
成功安装以下各版本
```
Installing collected packages: numpy, python-dateutil, kiwisolver, cycler, matplotlib
Successfully installed cycler-0.10.0 kiwisolver-1.1.0 matplotlib-3.1.3 numpy-1.18.1 python-dateutil-2.8.1
```



